using System;

namespace Contas.Infra.CrossCutting.Dtos.Inclusoes
{
    public class ContaInclusaoDto
    {
        public string Nome { get; set; }
        public DateTime Vencimento { get; set; }
        public DateTime Pagamento { get; set; }
        public decimal Valor { get; set; }
    }
}