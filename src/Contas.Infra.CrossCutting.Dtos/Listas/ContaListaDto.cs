using System;

namespace Contas.Infra.CrossCutting.Dtos.Listas
{
    public class ContaListaDto
    {
        public string Nome { get; set; }
        public decimal ValorOriginal { get; set; }
        public decimal ValorCorrigido { get; set; }
        public decimal QuantidadeDeDiasEmAtraso { get; set; }
        public DateTime DataDePagamento { get; set; }
    }
}