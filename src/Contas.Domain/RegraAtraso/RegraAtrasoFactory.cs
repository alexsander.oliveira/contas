using Contas.Domain.Interface.Regra;

namespace Contas.Domain.RegraAtraso
{
    public class RegraAtrasoFactory : IRegraAtrasoFactory
    {
        private IRegraAtraso Regra { get; set; }

        public IRegraAtraso CriarRegra(int diasEmAtraso) 
        {
            if ( diasEmAtraso <= 0 ) {
                return null;
            } else if ( diasEmAtraso <= 3 ) 
            {
                return new RegraAtrasoAteTresDias();
            } else if ( diasEmAtraso <= 5 )
            {
                return new RegraAtrasoAteCincoDias();
            } else {
                return new RegraAtrasoMaiorCincoDias();
            }
        }

    }
}