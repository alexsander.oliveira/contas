using Contas.Domain.Interface.Regra;

namespace Contas.Domain.RegraAtraso
{
    public class RegraAtrasoAteCincoDias : RegraAtrasoBase, IRegraAtraso
    {
        public RegraAtrasoAteCincoDias() : base(new Regra(3, 0.2M))
        {
        }
    }
}