using System;

namespace Contas.Domain.RegraAtraso
{
    public abstract class RegraAtrasoBase
    {
        private Regra RegraAplicada { get; set; }

        public RegraAtrasoBase(Regra regra)
        {
            RegraAplicada = regra;
        }

        public Tuple<decimal, Regra> CalcularMulta(
            int diasEmAtraso,
            decimal valorOriginal) {
            var valorCorrigido = valorOriginal;
            valorCorrigido += valorOriginal * RegraAplicada.PorcentagemMulta / 100;
            valorCorrigido += (valorOriginal * RegraAplicada.PorcentagemJuros / 100) * diasEmAtraso;

            return new Tuple<decimal, Regra>(Math.Round(valorCorrigido, 2), RegraAplicada);
        }
    }
}