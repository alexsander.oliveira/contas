using Contas.Domain.Interface.Regra;

namespace Contas.Domain.RegraAtraso
{
    public class RegraAtrasoMaiorCincoDias : RegraAtrasoBase, IRegraAtraso
    {
        public RegraAtrasoMaiorCincoDias() : base(new Regra(5, 0.3M))
        {
        }
    }
}