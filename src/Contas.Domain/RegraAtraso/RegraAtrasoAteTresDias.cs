using Contas.Domain.Interface.Regra;

namespace Contas.Domain.RegraAtraso
{
    public class RegraAtrasoAteTresDias : RegraAtrasoBase, IRegraAtraso
    {
        public RegraAtrasoAteTresDias() : base(new Regra(2, 0.1M))
        {
        }
    }
}