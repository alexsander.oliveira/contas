using System.Text.Json;

namespace Contas.Domain.RegraAtraso
{
    public class Regra
    {
        public Regra(decimal porcentagemMulta, decimal porcentagemJuros)
        {
            PorcentagemMulta = porcentagemMulta;
            PorcentagemJuros = porcentagemJuros;
        }

        public decimal PorcentagemMulta { get; private set; }
        public decimal PorcentagemJuros { get; private set; }

        public string ToJsonString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}