using System;
using FluentValidation;

namespace Contas.Domain.Entidades
{
    public class Conta : BaseEntity<Conta>
    {
        public Conta(
            string nome,
            DateTime vencimento,
            DateTime pagamento,
            decimal valorOriginal)
        {
            Nome = nome;
            Vencimento = vencimento;
            Pagamento = pagamento;
            ValorOriginal = valorOriginal;
            ValorCorrigido = ValorOriginal;
            CalcularDiasEmAtraso();
        }

        public string Nome { get; private set; }
        public DateTime Vencimento { get; private set; }
        public DateTime Pagamento { get; private set; }
        public decimal ValorOriginal { get; private set; }
        public decimal ValorCorrigido { get; private set; }
        public int DiasEmAtraso { get; private set; }
        public string RegraAplicada { get; private set; }

        private void CalcularDiasEmAtraso()
        {
            if ( Pagamento > Vencimento ) { 
                DiasEmAtraso = (int) Pagamento.Subtract(Vencimento).TotalDays;
            } else {
                DiasEmAtraso = 0;
            }
        }
        public void PreencherValorCorrigido(decimal valorCorrigido) 
        {
            ValorCorrigido = valorCorrigido;
        }
        public void PreencherRegraAplicada(string regraAplicada) 
        {
            RegraAplicada = regraAplicada;
        }

        public void Validar()
        {
            RuleFor(x => x.Nome)
            .NotEmpty()
            .WithMessage("O nome é obrigatório.");

            RuleFor(x => x.Vencimento)
            .NotEmpty()
            .WithMessage("A data de vencimento é obrigatório.");

            RuleFor(x => x.Vencimento)
            .Must(x => x >= new DateTime(1900, 1, 1))
            .WithMessage("A data de vencimento não pode ser inferior 01/01/1990.");

            RuleFor(x => x.Pagamento)
            .NotEmpty()
            .WithMessage("A data de pagamento é obrigatório.");

            RuleFor(x => x.Pagamento)
            .Must(x => x >= new DateTime(1900, 1, 1))
            .WithMessage("A data de pagamento não pode ser inferior 01/01/1990.");

            RuleFor(x => x.ValorOriginal)
            .Must(x => x > 0)
            .WithMessage("O valor não pode ser menor ou igual a zero.");
           
            ValidationResult = Validate(this);
        }
    }
}