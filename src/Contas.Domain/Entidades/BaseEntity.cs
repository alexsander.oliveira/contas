using System;
using FluentValidation;
using FluentValidation.Results;

namespace Contas.Domain.Entidades
{
    public abstract class BaseEntity<T> : AbstractValidator<T>
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid();
            ValidationResult = new ValidationResult();
        }
        
        public bool EhValido()
        {
            return this.ValidationResult.IsValid;
        }
        public Guid Id { get; protected set; }
        public ValidationResult ValidationResult { get; protected set; }
    }
}