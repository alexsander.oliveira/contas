using System.Collections.Generic;
using System.Linq;
using Contas.Domain.Entidades;
using Contas.Domain.Interface.Domain;
using Contas.Domain.Interface.Regra;
using Contas.Domain.Interface.Repository;
using Contas.Domain.Interface.Uow;
using Contas.Domain.Result;
using FluentValidation.Results;

namespace Contas.Domain.Service
{
    public class ContaService : IContaService
    {
        private readonly IContaRepository _contaRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRegraAtrasoFactory _regraAtrasoFactory;
        public ContaService(
            IContaRepository contaRepository,
            IUnitOfWork unitOfwork,
            IRegraAtrasoFactory regraAtraso) 
        {
            _unitOfWork = unitOfwork;
            _contaRepository = contaRepository;
            _regraAtrasoFactory = regraAtraso;
        }

        public Resultado IncluirConta(Conta conta)
        {
            conta.Validar();

            if(!conta.EhValido())
                return new Resultado(
                    false,
                    "Ocorreu um erro!",
                    GerarErrosDeValidacao(conta.ValidationResult));

            var regra = _regraAtrasoFactory.CriarRegra(conta.DiasEmAtraso);

            if ( regra != null ) {
                var resultadoCalculo = regra.CalcularMulta(conta.DiasEmAtraso, conta.ValorOriginal);
                conta.PreencherValorCorrigido(resultadoCalculo.Item1);
                conta.PreencherRegraAplicada(resultadoCalculo.Item2.ToJsonString());
            }

            _contaRepository.IncluirConta(conta);
            
            if ( _unitOfWork.Commit() ) {
                return new Resultado(true, "Conta incluída com sucesso!", null);
            } else {
                return new Resultado(false, "Ocorreu um erro!", null);
            }
        }

        private IEnumerable<Erro> GerarErrosDeValidacao(ValidationResult result)
        {
            return result.Errors.Select(x => new Erro(x.PropertyName, x.ErrorMessage)).AsEnumerable();
        }
    }
}