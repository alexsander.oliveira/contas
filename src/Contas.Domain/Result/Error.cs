namespace Contas.Domain.Result
{
    public class Erro
    {
        public Erro(string propriedade, string mensagem)
        {
            Propriedade = propriedade;
            Mensagem = mensagem;
        }
        public string Propriedade { get; private set; }
        public string Mensagem { get; private set; }
    }
}