using System.Collections.Generic;

namespace Contas.Domain.Result
{
    public class Resultado
    {
        public Resultado() { }

        public Resultado(bool success, string message, IEnumerable<Erro> erros)
        {
            Success = success;
            Message = message;
            Erros = erros;
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public IEnumerable<Erro> Erros { get; set; }
    }
}