namespace Contas.Domain.Interface.Uow
{
    public interface IUnitOfWork
    {
         bool Commit();
    }
}