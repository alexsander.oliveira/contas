using System.Collections.Generic;
using Contas.Infra.CrossCutting.Dtos.Listas;

namespace Contas.Domain.Interface.Query
{
    public interface IContaQuery
    {
         IEnumerable<ContaListaDto> ListarContas();
    }
}