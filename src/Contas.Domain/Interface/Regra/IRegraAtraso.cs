using System;

namespace Contas.Domain.Interface.Regra
{
    public interface IRegraAtraso
    {
         Tuple<decimal, Contas.Domain.RegraAtraso.Regra> CalcularMulta(int diasEmAtraso, decimal valorOriginal);
    }
}