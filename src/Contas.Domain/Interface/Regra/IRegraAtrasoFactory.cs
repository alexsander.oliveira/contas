namespace Contas.Domain.Interface.Regra
{
    public interface IRegraAtrasoFactory
    {
         IRegraAtraso CriarRegra(int diasEmAtraso);
    }
}