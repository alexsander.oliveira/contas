using Contas.Domain.Entidades;
using Contas.Domain.Result;

namespace Contas.Domain.Interface.Domain
{
    public interface IContaService
    {         
        Resultado IncluirConta(Conta conta);
    }
}