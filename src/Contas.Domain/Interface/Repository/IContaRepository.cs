using Contas.Domain.Entidades;

namespace Contas.Domain.Interface.Repository
{
    public interface IContaRepository
    {
         void IncluirConta(Conta conta);
    }
}