using System.Collections.Generic;
using Contas.Domain.Entidades;
using Contas.Domain.Interface.Domain;
using Contas.Domain.Interface.Query;
using Contas.Infra.CrossCutting.Dtos.Inclusoes;
using Contas.Infra.CrossCutting.Dtos.Listas;
using Microsoft.AspNetCore.Mvc;

namespace Contas.Api.Controllers
{
    public class ContaController : BaseController
    {
        private readonly IContaService _contaService;
        private readonly IContaQuery _contaQuery;
        public ContaController(
            IContaService contaService,
            IContaQuery query)
        {
            _contaService = contaService;
            _contaQuery = query;
        }

        [HttpPost]
        [Route("Inclui")]
        public IActionResult Incluir([FromBody]ContaInclusaoDto conta)
        {
            var result = _contaService.IncluirConta(new Conta(
                conta.Nome,
                conta.Vencimento,
                conta.Pagamento,
                conta.Valor));

            return Resultado(result);
        }

        [HttpGet]
        [Route("Lista")]
        public IEnumerable<ContaListaDto> Listar()
        {
            var result = _contaQuery.ListarContas();

            return result;
        }
    }
}