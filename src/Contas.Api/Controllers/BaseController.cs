using Contas.Domain.Result;
using Microsoft.AspNetCore.Mvc;

namespace Contas.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BaseController : ControllerBase
    {
        [NonAction]
        protected IActionResult Resultado(Resultado result)
        {
            if ( result.Success ) {
                return Ok(new
                {
                    success = true,
                    data = result.Message
                });
            } 
            else {
                return BadRequest(new 
                {
                    success = false,
                    message = result.Message,
                    erros = result.Erros
                });
            }
        }
    }
}