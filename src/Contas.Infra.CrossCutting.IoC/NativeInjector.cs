﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Contas.Infra.Data.Context;
using Contas.Domain.Interface.Repository;
using Contas.Infra.Data.Repository;
using Contas.Domain.Interface.Domain;
using Contas.Domain.Service;
using Contas.Domain.Interface.Query;
using Contas.Infra.Data.Query;
using Contas.Domain.Interface.Uow;
using Contas.Infra.Data.Uow;
using Contas.Domain.RegraAtraso;
using Contas.Domain.Interface.Regra;

namespace Contas.Infra.CrossCutting.IoC
{
    public class NativeInjector
    {
        public static void RegistrarServicos(IServiceCollection services)
        {
            // Domain
            services.AddScoped<IContaService, ContaService>();
            services.AddScoped<IRegraAtrasoFactory, RegraAtrasoFactory>();

            // Infra - Repository
            services.AddScoped<IContaRepository, ContaRepository>();

            // Infra - Query
            services.AddScoped<IContaQuery, ContaQuery>();

            // Infra - UnitOfWork
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<ContaContext>();
        }
    }
}
