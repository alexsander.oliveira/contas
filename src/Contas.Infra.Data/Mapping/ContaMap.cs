using Contas.Domain.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Contas.Infra.Data.Mapping
{
    public class ContaMap : IEntityTypeConfiguration<Conta>
    {
        public ContaMap() { }
        public void Configure(EntityTypeBuilder<Conta> builder)
        {
             #region Conta
            builder.Property(e => e.Id)
                .HasColumnName("ID")
                .ValueGeneratedNever();

            builder.Property(x => x.Pagamento)
                .HasColumnName("DATA_PAGAMENTO")
                .HasColumnType("datetime");

            builder.Property(x => x.Vencimento)
                .HasColumnName("DATA_VENCIMENTO")
                .HasColumnType("datetime");

            builder.Property(x => x.ValorOriginal)
                .HasColumnName("VALOR_ORIGINAL")
                .HasColumnType("numeric(15, 2)");
            
            builder.Property(x => x.ValorCorrigido)
                .HasColumnName("VALOR_CORRIGIDO")
                .HasColumnType("numeric(15, 2)");

            builder.Property(x => x.DiasEmAtraso)
                .HasColumnName("DIAS_EM_ATRASO")
                .HasColumnType("int");

            builder.Property(x => x.RegraAplicada)
                .HasColumnName("REGRA_APLICADA")
                .HasColumnType("varchar(150)");
            
            builder.Property(x => x.Nome)
                .HasColumnName("NOME")
                .HasColumnType("varchar(255)");

            builder.Ignore(x => x.ValidationResult);
            builder.Ignore(x => x.CascadeMode);
            
            builder.ToTable("Conta");
            #endregion
        }
    }
}