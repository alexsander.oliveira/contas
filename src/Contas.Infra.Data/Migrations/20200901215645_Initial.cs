﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Contas.Infra.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Conta",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    DATA_VENCIMENTO = table.Column<DateTime>(type: "datetime", nullable: false),
                    DATA_PAGAMENTO = table.Column<DateTime>(type: "datetime", nullable: false),
                    VALOR_ORIGINAL = table.Column<decimal>(type: "numeric(15, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conta", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Conta");
        }
    }
}
