﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Contas.Infra.Data.Migrations
{
    public partial class AddRegraAplicada : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "REGRA_APLICADA",
                table: "Conta",
                type: "varchar(150)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "REGRA_APLICADA",
                table: "Conta");
        }
    }
}
