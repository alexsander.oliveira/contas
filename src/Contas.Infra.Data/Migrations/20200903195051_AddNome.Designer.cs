﻿// <auto-generated />
using System;
using Contas.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Contas.Infra.Data.Migrations
{
    [DbContext(typeof(ContaContext))]
    [Migration("20200903195051_AddNome")]
    partial class AddNome
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.6")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Contas.Domain.Entidades.Conta", b =>
                {
                    b.Property<Guid>("Id")
                        .HasColumnName("ID")
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("DiasEmAtraso")
                        .HasColumnName("DIAS_EM_ATRASO")
                        .HasColumnType("int");

                    b.Property<string>("Nome")
                        .HasColumnName("NOME")
                        .HasColumnType("varchar(255)");

                    b.Property<DateTime>("Pagamento")
                        .HasColumnName("DATA_PAGAMENTO")
                        .HasColumnType("datetime");

                    b.Property<string>("RegraAplicada")
                        .HasColumnName("REGRA_APLICADA")
                        .HasColumnType("varchar(150)");

                    b.Property<decimal>("ValorCorrigido")
                        .HasColumnName("VALOR_CORRIGIDO")
                        .HasColumnType("numeric(15, 2)");

                    b.Property<decimal>("ValorOriginal")
                        .HasColumnName("VALOR_ORIGINAL")
                        .HasColumnType("numeric(15, 2)");

                    b.Property<DateTime>("Vencimento")
                        .HasColumnName("DATA_VENCIMENTO")
                        .HasColumnType("datetime");

                    b.HasKey("Id");

                    b.ToTable("Conta");
                });
#pragma warning restore 612, 618
        }
    }
}
