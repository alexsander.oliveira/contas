﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Contas.Infra.Data.Migrations
{
    public partial class DiasAtrasoEValorCorrigido : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DIAS_EM_ATRASO",
                table: "Conta",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "VALOR_CORRIGIDO",
                table: "Conta",
                type: "numeric(15, 2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DIAS_EM_ATRASO",
                table: "Conta");

            migrationBuilder.DropColumn(
                name: "VALOR_CORRIGIDO",
                table: "Conta");
        }
    }
}
