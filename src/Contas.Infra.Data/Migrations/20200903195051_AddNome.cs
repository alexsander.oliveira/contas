﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Contas.Infra.Data.Migrations
{
    public partial class AddNome : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NOME",
                table: "Conta",
                type: "varchar(255)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NOME",
                table: "Conta");
        }
    }
}
