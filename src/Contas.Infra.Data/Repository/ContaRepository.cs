using Contas.Domain.Entidades;
using Contas.Domain.Interface.Repository;
using Contas.Infra.Data.Context;

namespace Contas.Infra.Data.Repository
{
    public class ContaRepository : IContaRepository
    {
        private ContaContext _context;
        public ContaRepository(ContaContext context)
        {
            _context = context;
        }
        public void IncluirConta(Conta conta)
        {
            _context.Conta.Add(conta);
        }
    }
}