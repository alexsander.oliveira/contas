using Contas.Domain.Interface.Uow;
using Contas.Infra.Data.Context;

namespace Contas.Infra.Data.Uow
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ContaContext _context;
        public UnitOfWork(ContaContext context)
        {
            _context = context;
        }
        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}