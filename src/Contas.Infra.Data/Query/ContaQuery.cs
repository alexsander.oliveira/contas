using System.Collections.Generic;
using System.Data;
using Contas.Domain.Interface.Query;
using Contas.Infra.CrossCutting.Dtos.Listas;
using Contas.Infra.Data.Context;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Contas.Infra.Data.Query
{
    public class ContaQuery : IContaQuery
    {
        private IDbConnection _connection;
        private ContaContext _context;
        public ContaQuery(ContaContext context)
        {
            _context = context;
            _connection = _context.Database.GetDbConnection();
        }

        public IEnumerable<ContaListaDto> ListarContas()
        {
            var builder = new SqlBuilder();

            var template = builder.AddTemplate("SELECT TOP 1500 /**select**/ FROM Conta C /**where**/ /**orderby**/");

            //Select
            builder.Select($"C.NOME AS {nameof(ContaListaDto.Nome)}");
            builder.Select($"C.VALOR_CORRIGIDO AS {nameof(ContaListaDto.ValorCorrigido)}");
            builder.Select($"C.VALOR_ORIGINAL AS {nameof(ContaListaDto.ValorOriginal)}");
            builder.Select($"C.DATA_PAGAMENTO AS {nameof(ContaListaDto.DataDePagamento)}");
            builder.Select($"C.DIAS_EM_ATRASO AS {nameof(ContaListaDto.QuantidadeDeDiasEmAtraso)}");

            //Order by
            builder.OrderBy("C.DATA_PAGAMENTO");

            return _connection.Query<ContaListaDto>(template.RawSql, template.Parameters);
        }

    }
}