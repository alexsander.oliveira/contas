using Contas.Domain.Entidades;
using Contas.Infra.Data.Mapping;
using Microsoft.EntityFrameworkCore;

namespace Contas.Infra.Data.Context
{
    public class ContaContext : DbContext
    {
        public ContaContext(DbContextOptions<ContaContext> options)
        :base(options)
        {   }
        public DbSet<Conta> Conta { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           modelBuilder.ApplyConfiguration(new ContaMap());
        }
    }
}