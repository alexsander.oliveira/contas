import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContaModule } from './conta/conta.module';
import { ContaFormComponent } from './conta/conta-form/conta-form.component';
import { ContaListaComponent } from './conta/conta-lista/conta-lista.component';


const routes: Routes = [
  {
    path: 'conta-novo',
    component: ContaFormComponent
  },
  {
    path: '',
    component: ContaListaComponent
  },
  {
    path: 'conta-lista',
    component: ContaListaComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    ContaModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
