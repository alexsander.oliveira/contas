export class ContaInclusao {
    nome: string;
    valor: number;
    vencimento: Date;
    pagamento: Date;
}

export class ResultadoInclusao {
    success: boolean;
    data: string;
    erros: any;
}
