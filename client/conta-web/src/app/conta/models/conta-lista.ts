
export class ContaLista {
    nome: string;
    valorOriginal: number;
    valorCorrigido: number;
    quantidadeDeDiasEmAtraso: number;
    dataDePagamento: Date;
}
