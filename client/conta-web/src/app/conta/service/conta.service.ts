import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContaLista } from '../models/conta-lista';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ContaInclusao, ResultadoInclusao } from '../models/conta-inclusao';

@Injectable({
  providedIn: 'root'
})
export class ContaService {

  constructor(private http: HttpClient) { }

  listarContas(): Observable<ContaLista[]> {
    return this.http.get<ContaLista[]>(environment.serverUrl + 'Conta/Lista');
  }

  incluirConta(conta: ContaInclusao): Observable<ResultadoInclusao> {
    return this.http.post<ResultadoInclusao>(environment.serverUrl + 'Conta/Inclui', conta);
  }

}
