import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContaService } from '../service/conta.service';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-conta-form',
  templateUrl: './conta-form.component.html',
  styleUrls: ['./conta-form.component.css']
})
export class ContaFormComponent implements OnInit, OnDestroy {
  public formGroup: FormGroup;
  submitted: boolean;
  contaServicoSubscription: Subscription;
  erros: any[];
  mensagemSucesso: string;

  constructor(
    private formBuilder: FormBuilder,
    private contaService: ContaService,
    private router: Router
    ) {

    this.formGroup = this.formBuilder.group({
      nome: ['', [Validators.required]],
      valor: ['', [Validators.required]],
      vencimento: ['', [Validators.required]],
      pagamento: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.erros = [];
  }

  salvar() {
    this.submitted = true;
    if ( !this.formGroup.valid ) {
      return;
    }

    const conta = Object.assign({}, this.formGroup.value);

    conta.pagamento = moment(conta.pagamento).format('YYYY-MM-DDT00:00:00.000') + 'Z';
    conta.vencimento = moment(conta.vencimento).format('YYYY-MM-DDT00:00:00.000') + 'Z';
    conta.valor = Number(conta.valor);

    this.contaServicoSubscription = this.contaService.incluirConta(conta).subscribe(
      (res) => {
        if ( res.success ) {
          this.mensagemSucesso = res.data;
          this.formGroup.reset();
          this.submitted = false;
        }
      }, (error) => {
          if ( error && error.error.erros ) {
            this.erros = error.error.erros;
          }
      });
  }

  voltar() {
    this.router.navigate(['conta-lista']);
  }

  ngOnDestroy() {
    if ( this.contaServicoSubscription ) {
      this.contaServicoSubscription.unsubscribe();
    }
  }
}
