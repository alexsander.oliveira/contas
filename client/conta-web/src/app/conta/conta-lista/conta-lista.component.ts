import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContaService } from '../service/conta.service';
import { ContaLista } from '../models/conta-lista';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-conta-lista',
  templateUrl: './conta-lista.component.html',
  styleUrls: ['./conta-lista.component.css']
})
export class ContaListaComponent implements OnInit, OnDestroy {

  contas: ContaLista[];
  contaSubscription: Subscription;

  constructor(private service: ContaService, private router: Router) { }

  ngOnInit() {
    this.contas = [];
    this.obterContas();
  }

  obterContas() {
    this.contaSubscription = this.service.listarContas()
    .subscribe((res) => {
      this.contas = res;
    });
  }

  cadastro() {
    this.router.navigate(['conta-novo']);
  }

  ngOnDestroy(): void {
    if ( this.contaSubscription ) {
      this.contaSubscription.unsubscribe();
    }
  }
}
