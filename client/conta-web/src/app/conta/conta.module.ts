import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContaFormComponent } from './conta-form/conta-form.component';
import { ContaListaComponent } from './conta-lista/conta-lista.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule, BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { AlertModule } from 'ngx-bootstrap/alert';

export function getDatepickerConfig(): BsDatepickerConfig {
  return Object.assign(new BsDatepickerConfig(), {
    dateInputFormat: 'YYYY-MM-DD',
    showWeekNumbers: false
  });
}

@NgModule({
  declarations: [
    ContaFormComponent,
    ContaListaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    AlertModule.forRoot()
  ],
  providers: [
    { provide: BsDatepickerConfig, useFactory: getDatepickerConfig }
  ],
})
export class ContaModule { }
