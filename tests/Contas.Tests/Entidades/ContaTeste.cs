using System;
using System.Linq;
using Contas.Domain.Entidades;
using Xunit;

namespace Contas.Tests
{
    public class ContaTeste
    {
        [Fact]
        public void EntidadeContaComValoresValida()
        {
            // Arrange
            var conta = new Conta(
                "Teste",
                new DateTime(2020, 1, 1),
                new DateTime(2020, 1, 1),
                600);

            //Act
            conta.Validar();

            //Assert
            Assert.True(conta.EhValido(), "A entidade conta não corresponde a uma entidade válida");
        }

        [Fact]
        public void EntidadeContaComValorOriginalInvalido()
        {
            // Arrange
            var conta = new Conta(
                "Teste",
                new DateTime(2020, 1, 1),
                new DateTime(2020, 1, 1),
                0);

            //Act
            conta.Validar();

            //Assert
            Assert.False(conta.EhValido(), 
                "A entidade conta não corresponde a uma entidade inválida");
            Assert.True(conta.ValidationResult.Errors.Any(),
                "A entidade conta não retornou nenhum erro");
            Assert.True(conta.ValidationResult.Errors.Any(x => x.ErrorMessage == "O valor não pode ser menor ou igual a zero."),
                "A entidade conta não retornou o erro esperado");
        }

        [Fact]
        public void EntidadeContaComDatasInvalidas()
        {
            // Arrange
            var conta = new Conta(
                "Teste",
                new DateTime(1899, 1, 1),
                new DateTime(1899, 1, 1),
                6554);

            //Act
            conta.Validar();

            //Assert
            Assert.False(conta.EhValido(), 
                "A entidade conta não corresponde a uma entidade inválida");
            Assert.True(conta.ValidationResult.Errors.Any(),
                "A entidade conta não retornou nenhum erro");
            Assert.True(conta.ValidationResult.Errors.Any(x => x.ErrorMessage == "A data de pagamento não pode ser inferior 01/01/1990."),
                "A entidade conta não retornou o erro esperado");
            Assert.True(conta.ValidationResult.Errors.Any(x => x.ErrorMessage == "A data de vencimento não pode ser inferior 01/01/1990."),
                "A entidade conta não retornou o erro esperado");
        }
    }
}
