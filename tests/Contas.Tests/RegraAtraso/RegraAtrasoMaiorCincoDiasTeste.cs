using Contas.Domain.RegraAtraso;
using Xunit;

namespace Contas.Tests.RegraAtraso
{
    public class RegraAtrasoMaiorCincoDiasTeste
    {
        [Fact]
        public void RegraAtrasoMaiorCincoDias()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoMaiorCincoDias();
            //Act
            var regra = regraAtraso.CalcularMulta(7, 500);

            //Assert
            Assert.True(regra.Item1 == 535.5M, "A RegraAtrasoMaiorCincoDias não retornou o resultado esperado.");
            Assert.NotNull(regra.Item2);
        }

        [Fact]
        public void RegraAtrasoMaiorCincoDiasValorDiferente()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoMaiorCincoDias();
            //Act
            var regra = regraAtraso.CalcularMulta(7, 520.50M);

            //Assert
            Assert.True(regra.Item1 == 557.46M, "A RegraAtrasoMaiorCincoDias com valor diferente não retornou o resultado esperado.");
            Assert.NotNull(regra.Item2);
        }
    }
}