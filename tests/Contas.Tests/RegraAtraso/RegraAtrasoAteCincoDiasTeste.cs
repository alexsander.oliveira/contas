using Contas.Domain.RegraAtraso;
using Xunit;

namespace Contas.Tests.RegraAtraso
{
    public class RegraAtrasoAteCincoDiasTeste
    {
        [Fact]
        public void RegraAtrasoAteCincoDias()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoAteCincoDias();
            //Act
            var regra = regraAtraso.CalcularMulta(5, 500);

            //Assert
            Assert.True(regra.Item1 == 520, "A RegraAtrasoAteCincoDias não retornou o resultado esperado.");
            Assert.NotNull(regra.Item2);
        }

        [Fact]
        public void RegraAtrasoAteCincoDiasValorDiferente()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoAteCincoDias();
            //Act
            var regra = regraAtraso.CalcularMulta(5, 520.50M);

            //Assert
            Assert.True(regra.Item1 == 541.32M, "A RegraAtrasoAteCincoDias com valor diferente não retornou o resultado esperado.");
            Assert.NotNull(regra.Item2);
        }
    }
}