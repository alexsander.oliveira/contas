using System;
using Contas.Domain.Entidades;
using Contas.Domain.Interface.Regra;
using Contas.Domain.RegraAtraso;
using Xunit;

namespace Contas.Tests.RegraAtraso
{
    public class RegraAtrasoFactoryTeste
    {
        [Fact]
        public void RegraAtrasoFactoryRetornoNull()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoFactory();
            //Act
            var regra = regraAtraso.CriarRegra(0);

            //Assert
            Assert.True(regra == null, "A RegraAtrasoFactory não retornou o resultado esperado.");
        }

        [Fact]
        public void RegraAtrasoFactoryTresDias()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoFactory();
            //Act
            var regra = regraAtraso.CriarRegra(3);

            //Assert
            Assert.IsType<RegraAtrasoAteTresDias>(regra);
        }

        [Fact]
        public void RegraAtrasoFactoryCincoDias()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoFactory();
            //Act
            var regra = regraAtraso.CriarRegra(5);

            //Assert
            Assert.IsType<RegraAtrasoAteCincoDias>(regra);
        }

        [Fact]
        public void RegraAtrasoFactoryMaiorQueCincoDias()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoFactory();

            //Act
            var regra = regraAtraso.CriarRegra(25);

            //Assert
            Assert.IsType<RegraAtrasoMaiorCincoDias>(regra);
        }
    }
}