using Contas.Domain.RegraAtraso;
using Xunit;

namespace Contas.Tests.RegraAtraso
{
    public class RegraAtrasoAteTresDiasTeste
    {
        [Fact]
        public void RegraAtrasoAteTresDias()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoAteTresDias();
            //Act
            var regra = regraAtraso.CalcularMulta(3, 500);

            //Assert
            Assert.True(regra.Item1 == 511.5M, "A RegraAtrasoAteTresDias não retornou o resultado esperado.");
            Assert.NotNull(regra.Item2);
        }

        [Fact]
        public void RegraAtrasoAteTresDiasValorDiferente()
        {
            // Arrange
            var regraAtraso = new RegraAtrasoAteTresDias();
            //Act
            var regra = regraAtraso.CalcularMulta(3, 520.50M);

            //Assert
            Assert.True(regra.Item1 == 532.47M, "A RegraAtrasoAteTresDias com valor diferente não retornou o resultado esperado.");
            Assert.NotNull(regra.Item2);
        }
    }
}