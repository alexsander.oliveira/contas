using System;
using System.Linq;
using Contas.Domain.Entidades;
using Contas.Domain.Interface.Regra;
using Contas.Domain.Interface.Repository;
using Contas.Domain.Interface.Uow;
using Contas.Domain.RegraAtraso;
using Contas.Domain.Service;
using Moq;
using Xunit;

namespace Contas.Tests.Servico
{
    public class ContaServiceTeste
    {
        private readonly Mock<IContaRepository> _contaRepositoryMock;
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<IRegraAtrasoFactory> _regraAtrasoMock;

        public ContaServiceTeste()
        {
            _contaRepositoryMock = new Mock<IContaRepository>();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _regraAtrasoMock = new Mock<IRegraAtrasoFactory>();
        }

        [Fact]
        public void IncluirContaServiceComSucesso()
        {
            // Arrange
            _unitOfWorkMock.Setup(x => x.Commit()).Returns(true);
            _regraAtrasoMock.Setup(x => x.CriarRegra(3))
                .Returns(new RegraAtrasoAteTresDias());
            var conta = new Conta(
                "Teste",
                new DateTime(2020, 1, 1),
                new DateTime(2020, 1, 4),
                500);

            var contaService = new ContaService(
                _contaRepositoryMock.Object,
                _unitOfWorkMock.Object,
                _regraAtrasoMock.Object);
            //Act
            var resultado = contaService.IncluirConta(conta);

            //Assert
            Assert.True(resultado.Success, "A ContaService não retornou o resultado esperado.");
        }

        [Fact]
        public void IncluirContaServiceComErroNasValidacoes()
        {
            // Arrange
            var conta = new Conta(
                "Teste",
                new DateTime(2020, 1, 1),
                new DateTime(2020, 1, 4),
                0);

            var contaService = new ContaService(
                _contaRepositoryMock.Object,
                _unitOfWorkMock.Object,
                _regraAtrasoMock.Object);
            //Act
            var resultado = contaService.IncluirConta(conta);

            //Assert
            Assert.False(resultado.Success, "A ContaService com erro nas validações não retornou o resultado esperado.");
            Assert.True(resultado.Erros.Any(), "A ContaService com erro nas validações não retornou nenhum erro esperado.");
        }

        [Fact]
        public void IncluirContaServiceComFalhaAoSalvar()
        {
            // Arrange
            _unitOfWorkMock.Setup(x => x.Commit()).Returns(false);
            _regraAtrasoMock.Setup(x => x.CriarRegra(3))
                .Returns(new RegraAtrasoAteTresDias());
            var conta = new Conta(
                "Teste",
                new DateTime(2020, 1, 1),
                new DateTime(2020, 1, 4),
                500);

            var contaService = new ContaService(
                _contaRepositoryMock.Object,
                _unitOfWorkMock.Object,
                _regraAtrasoMock.Object);
            //Act
            var resultado = contaService.IncluirConta(conta);

            //Assert
            Assert.False(resultado.Success, "A ContaService com falha ao salvar não retornou o resultado esperado.");
            Assert.True(resultado.Message == "Ocorreu um erro!", "A ContaService com falha ao salvar não retornou o resultado esperado.");
        }
    }
}