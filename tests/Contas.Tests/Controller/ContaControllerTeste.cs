using System;
using Contas.Api.Controllers;
using Contas.Domain.Entidades;
using Contas.Domain.Interface.Domain;
using Contas.Domain.Interface.Query;
using Contas.Domain.Result;
using Contas.Infra.CrossCutting.Dtos.Inclusoes;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Contas.Tests.Controller
{
    public class ContaControllerTeste
    {
        private readonly Mock<IContaService> _contaServiceMock;
        private readonly Mock<IContaQuery> _contaQueryMock;

        public ContaControllerTeste()
        {
            _contaServiceMock = new Mock<IContaService>();
            _contaQueryMock = new Mock<IContaQuery>();
        }

        [Fact]
        public void IncluirContaServiceComSucesso()
        {
            // Arrange
            _contaServiceMock.Setup(x => x.IncluirConta(It.IsAny<Conta>()))
            .Returns(new Resultado(true, "Conta incluída com sucesso!", null));
            var contaDto = new ContaInclusaoDto() {
                Pagamento = new DateTime(2020, 1, 1),
                Vencimento = new DateTime(2020, 1, 4),
                Valor = 500
            };

            var contaController = new ContaController(
                _contaServiceMock.Object,
                _contaQueryMock.Object);
            //Act
            var resultado = contaController.Incluir(contaDto);

            //Assert
            Assert.IsType<OkObjectResult>(resultado);
        }

        [Fact]
        public void IncluirContaServiceComFalha()
        {
            // Arrange
            _contaServiceMock.Setup(x => x.IncluirConta(It.IsAny<Conta>()))
            .Returns(new Resultado(false, "Ocorreu um erro!", null));
            var contaDto = new ContaInclusaoDto() {
                Pagamento = new DateTime(2020, 1, 1),
                Vencimento = new DateTime(2020, 1, 4),
                Valor = 500 
            };

            var contaController = new ContaController(
                _contaServiceMock.Object,
                _contaQueryMock.Object
                );
            //Act
            var resultado = contaController.Incluir(contaDto);

            //Assert
            Assert.IsType<BadRequestObjectResult>(resultado);
        }
    }
}